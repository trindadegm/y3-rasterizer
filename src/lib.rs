#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct Vector4([f32; 4]);

pub type Vertex = Vector4;

#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct Vector2([f32; 2]);

#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct PixelCoord([u32; 2]);

#[derive(Debug)]
pub enum RasterError {
    ViewportTooBig,
    ViewportSizeMismatch,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Poly2Fn { a: f32, b: f32 }

pub type RasterResult<T> = Result<T, RasterError>;

pub struct Rasterizer {
    dim: (u32, u32),
}

impl Rasterizer {
    /// - `viewport_dim`: the dimensions of the viewport, as `(width, height)`,
    ///   in pixels.
    pub fn new(viewport_dim: (u32, u32)) -> RasterResult<Self> {
        Self::_viewport_size(viewport_dim).and_then(|size| {
            // Multiply by four because each pixel has four bytes
            size.checked_mul(4)
                // If the byte length of the viewport buffer fits on a usize
                // thent we know the viewport is not too big
                .map(|_buf_size| Self { dim: viewport_dim })
                // Otherwise we won't create the Rasterizer because the
                // viewport is much too big, and we'll have problems on the
                // calculations bellow (wrap-arounds and overflows).
                .ok_or(RasterError::ViewportTooBig)
        })
    }

    #[inline]
    fn _viewport_size(dim: (u32, u32)) -> RasterResult<usize> {
        let w: usize = dim.0.try_into().map_err(|_| RasterError::ViewportTooBig)?;
        let h: usize = dim.1.try_into().map_err(|_| RasterError::ViewportTooBig)?;
        w.checked_mul(h).ok_or(RasterError::ViewportTooBig)
    }

    #[inline]
    pub fn viewport_size(&self) -> usize {
        Self::_viewport_size(self.dim).unwrap()
    }

    #[inline]
    fn pix_coords(&self, line: u32, column: u32) -> usize {
        let l = line as usize;
        let c = column as usize;
        let line_len = self.dim.0 as usize;
        l * line_len + c
    }

    /// Projects a vertex to a pixel in the viewport. The projection is
    /// orthogonal to the view plane.
    ///
    /// Returns `None` if the pixel would be outside of the viewport.
    fn project_to_pixel(&self, vertex: &Vector2) -> Option<usize> {
        let [x, y] = [vertex.x(), vertex.y()];
        // Normalize coords from [-1.0, +1.0] to [0.0, 1.0]
        let x = (x + 1.0) / 2.0;
        let y = (y + 1.0) / 2.0;

        let pix_x = x * (self.dim.0) as f32;
        let pix_y = y * (self.dim.1) as f32;

        let pix_x = if pix_x >= 0.0 && pix_x < self.dim.0 as f32 {
            pix_x as u32
        } else {
            return None;
        };
        let pix_y = if pix_y >= 0.0 && pix_y < self.dim.1 as f32 {
            pix_y as u32
        } else {
            return None;
        };

        Some(self.pix_coords(pix_y, pix_x))
    }

    /// Returns `None` if the pixel would be outside of the viewport.
    fn ftou_x(&self, x: f32) -> Option<u32> {
        let x = (x + 1.0) / 2.0;
        let pix_x = x * (self.dim.0) as f32;
        if pix_x >= 0.0 && pix_x < self.dim.0 as f32 {
            Some(pix_x as u32)
        } else {
            None
        }
    }
    /// Returns `None` if the pixel would be outside of the viewport.
    fn ftou_y(&self, y: f32) -> Option<u32> {
        let y = (y + 1.0) / 2.0;
        let pix_y = y * (self.dim.1) as f32;
        if pix_y >= 0.0 && pix_y < self.dim.1 as f32 {
            Some(pix_y as u32)
        } else {
            None
        }
    }
    fn utof_x(&self, column: u32) -> f32 {
        let x = (column as f32)/(self.dim.0 as f32);
        2.0 * x - 1.0
    }
    fn utof_y(&self, line: u32) -> f32 {
        let y = (line as f32)/(self.dim.1 as f32);
        2.0 * y - 1.0
    }

    fn pixel_to_point(&self, pixel_index: usize) -> Vector2 {
        let px_y = pixel_index / self.dim.0 as usize;
        let px_x = pixel_index % self.dim.0 as usize;
        let px_xf = px_x as f32;
        let px_yf = px_y as f32;
        let x = (2.0 * px_xf) / (self.dim.0 as f32) - 1.0;
        let y = (2.0 * px_yf) / (self.dim.1 as f32) - 1.0;
        Vector2::new(x, y)
    }

    pub fn triangles_indexed(&self, buf: &mut [u8], vertices: &[Vector4], indices: &[u16]) -> RasterResult<()> {
        if buf.len() != (self.viewport_size() * 4) {
            return Err(RasterError::ViewportSizeMismatch);
        }

        indices
            .chunks_exact(3)
            .for_each(|three_indices| {
                let three_indices: [u16; 3] = three_indices.try_into().unwrap();
                let idx = three_indices.map(|i| i as usize);
                let arr = [vertices[idx[0]], vertices[idx[1]], vertices[idx[2]]];
                self.raster_triangle(buf, &arr)
            });

        Ok(())
    }

    fn raster_triangle(&self, buf: &mut [u8], [v1, v2, v3]: &[Vector4; 3]) {
        use std::mem::swap;
        let [mut v1, mut v2, mut v3] = [v1.project2(), v2.project2(), v3.project2()];
        // Order v1, v2, v3 so that v1.y() < v2.y() < v3.y()
        if v1.y() > v2.y() {
            swap(&mut v1, &mut v2);
        }
        if v1.y() > v3.y() {
            swap(&mut v1, &mut v3);
        }
        if v2.y() > v3.y() {
            swap(&mut v2, &mut v3);
        }
        let begin_y = f32::clamp(v1.y(), -1.0, 1.0);
        let end_y = f32::clamp(v2.y(), -1.0, 1.0);

        let begin_pix_y = self.ftou_y(begin_y).unwrap();
        let end_pix_y = self.ftou_y(end_y).unwrap();

        if end_y > begin_y {
            let to_v2 = Poly2Fn::passing_through_yx(v1, v2);
            let to_v3 = Poly2Fn::passing_through_yx(v1, v3);
            let (left_fn, right_fn) = if to_v2.coef_a() < to_v3.coef_a() {
                (to_v2, to_v3)
            } else {
                (to_v3, to_v2)
            };

            self.partial_raster_triangle(buf, left_fn, right_fn, begin_pix_y, end_pix_y);
        }

        if v3.y() > end_y {
            // Update v1 (the highest point )to the "middle" of the v1 v3 line,
            // using the v2 point to decide where to stop
            let v1 = {
                let c = Poly2Fn::passing_through_yx(v1, v3).map(v2.y() - v1.y());
                Vector2::new(c, v2.y())
            };
            let from_v1 = Poly2Fn::passing_through_yx(v1, v3);
            let from_v2 = Poly2Fn::passing_through_yx(v2, v3);
            let (left_fn, right_fn) = if v1.x() < v2.x() {
                (from_v1, from_v2)
            } else {
                (from_v2, from_v1)
            };

            let begin_pix_y = end_pix_y;
            let end_y = f32::clamp(v3.y(), -1.0, 1.0);
            let end_pix_y = self.ftou_y(end_y).unwrap();

            self.partial_raster_triangle(buf, left_fn, right_fn, begin_pix_y, end_pix_y);
        }
    }

    fn partial_raster_triangle(
        &self,
        buf: &mut [u8],
        left_fn: Poly2Fn,
        right_fn: Poly2Fn,
        begin_pix_y: u32,
        end_pix_y: u32,
    ) {
        let root_y = self.utof_y(begin_pix_y);

        for draw_pix_y in begin_pix_y..=end_pix_y {
            let draw_y = self.utof_y(draw_pix_y);

            // The max is to avoid a small negative y that may happen because of
            // floating point imprecision.
            let plot_y = f32::max(draw_y - root_y, 0.0);
            let begin_x = f32::clamp(left_fn.map(plot_y), -1.0, 1.0);
            let end_x = f32::clamp(right_fn.map(plot_y), -1.0, 1.0);
            let begin_pix_x = self.ftou_x(begin_x).unwrap();
            let end_pix_x = self.ftou_x(end_x).unwrap();
            let begin_pix = self.pix_coords(draw_pix_y, begin_pix_x);
            let end_pix = self.pix_coords(draw_pix_y, end_pix_x);

            paint_buf_range(buf, begin_pix, end_pix);
        }
    }
}

fn paint_buf_range(buf: &mut [u8], pix_begin: usize, pix_end: usize) {
    for pix in pix_begin..=pix_end {
        let pix_index = pix * 4;
        buf[pix_index] = 255;
        buf[pix_index + 1] = 0;
        buf[pix_index + 2] = 0;
        buf[pix_index + 3] = 255;
    }
}

impl Poly2Fn {
    #[inline]
    pub fn new(a: f32, b: f32) -> Self {
        Self { a, b }
    }

    pub fn passing_through_yx(pt0: Vector2, pt1: Vector2) -> Self {
        let ratio = {
            let delta_x = pt1.x() - pt0.x();
            let delta_y = pt1.y() - pt0.y();
            delta_x / delta_y
        };
        Self::new(ratio, pt0.x())
    }

    #[inline(always)]
    pub fn map(self, x: f32) -> f32 {
        x * self.a + self.b
    }

    #[inline(always)]
    pub fn coef_a(self) -> f32 {
        self.a
    }

    #[inline(always)]
    pub fn coef_b(self) -> f32 {
        self.b
    }
}

impl Vector4 {
    #[inline]
    pub fn new(x: f32, y: f32, z: f32, w: f32) -> Self {
        Self([x, y, z, w])
    }

    #[inline]
    pub fn x(&self) -> f32 {
        self.0[0]
    }

    #[inline]
    pub fn y(&self) -> f32 {
        self.0[1]
    }

    #[inline]
    pub fn z(&self) -> f32 {
        self.0[2]
    }

    #[inline]
    pub fn w(&self) -> f32 {
        self.0[3]
    }

    #[inline]
    pub fn project2(&self) -> Vector2 {
        Vector2::new(self.x() / self.w(), self.y() / self.w())
    }
}

impl Vector2 {
    #[inline]
    pub fn new(x: f32, y: f32) -> Self {
        Self([x, y])
    }

    #[inline]
    pub fn x(&self) -> f32 {
        self.0[0]
    }

    #[inline]
    pub fn y(&self) -> f32 {
        self.0[1]
    }
}

impl PixelCoord {
    #[inline]
    pub fn new(x: u32, y: u32) -> Self {
        Self([x, y])
    }

    #[inline]
    pub fn x(&self) -> u32 {
        self.0[0]
    }

    #[inline]
    pub fn y(&self) -> u32 {
        self.0[1]
    }
}
