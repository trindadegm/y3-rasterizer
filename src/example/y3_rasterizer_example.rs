use winit::{
    dpi::PhysicalSize,
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};
use y3_rasterizer::{Rasterizer, Vector4};

pub enum RenderEvent {}

const WINDOW_DIMENSIONS: (u32, u32) = (800, 450);

fn main() {
    let event_loop: EventLoop<RenderEvent> = EventLoop::with_user_event();

    let (win_w, win_h) = WINDOW_DIMENSIONS;

    let window = WindowBuilder::new()
        .with_title("Rasterizer example")
        .with_inner_size(PhysicalSize::new(win_w, win_h))
        .with_resizable(false)
        .build(&event_loop)
        .unwrap_or_else(|err| {
            eprintln!("Failed to create window: {err}");
            std::process::exit(1)
        });

    let surface = pixels::SurfaceTexture::new(win_w, win_h, &window);
    let mut viewport = pixels::Pixels::new(win_w, win_h, surface).unwrap_or_else(|err| {
        eprintln!("Failed to create pixel canvas: {err}");
        std::process::exit(1)
    });

    viewport.set_clear_color(pixels::wgpu::Color {
        r: 0.0,
        g: 0.0,
        b: 0.0,
        a: 1.0,
    });

    let rasterizer = Rasterizer::new(WINDOW_DIMENSIONS).unwrap_or_else(|err| {
        eprintln!("Failed to create rasterizer: {err:?}");
        std::process::exit(1)
    });

    crossbeam::scope(|thread_scope| {
        thread_scope.spawn(|_thread_scope| {
            let frame = viewport.get_frame();
            rasterizer
                .triangles_indexed(
                    frame,
                    &[
                        Vector4::new(0.2, -0.75, 0.0, 1.0),
                        Vector4::new(-0.5, -0.9, 0.0, 1.0),
                        Vector4::new(0.5, 0.9, 0.0, 1.0),
                        Vector4::new(0.6, 0.4, 0.0, 1.0),
                    ],
                    &[0, 1, 2],
                    // &[0, 1, 2, 2, 3, 0],
                )
                .unwrap();

            viewport.render().unwrap_or_else(|err| {
                eprintln!("Failed to render to viewport: {err}");
                std::process::exit(1)
            });
        });

        event_loop.run(|event, _window_target, control_flow| {
            *control_flow = ControlFlow::Wait;
            match event {
                Event::WindowEvent { event, .. } => match event {
                    WindowEvent::CloseRequested => {
                        *control_flow = ControlFlow::Exit;
                    }
                    _ => {}
                },
                _ => {}
            }
        })
    })
    .unwrap_or_else(|_err| {
        eprintln!("Some thread error occurred");
        std::process::exit(1)
    });
}
